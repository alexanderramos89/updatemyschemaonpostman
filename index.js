const fs = require('fs')
const config = require('config-yml');
const collection_uid = config.collection.uid;
const api_key = config.apikey.value;
fs.readFile('./petstore.json', 'utf8', (err, jsonString) => {
    if (err) {
        console.log("Error reading file from disk:", err)
        return
    }
    try {
        const newman = require('newman'); // require newman in your project
        console.log(jsonString);
        newman.run({
                        collection: "https://api.getpostman.com/collections/" + collection_uid + "?apikey=" + api_key, 
                        globalVar: [ 
                                    { "key": "schema", "value": jsonString}, {"key": "apikey", "value": api_key} 
],
                        reporters: 'cli'
                    },  function (err) {

                                            if (err) { throw err; }
                                            console.log('collection run complete!');
                                        });
                    }
    catch(err) {
                        console.log('Error parsing JSON string:', err)
                }
})


